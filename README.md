# sandman

Sandman is a wrapper around (pkgsrc) sandboxctl.
It aims to bring a little bit of container to sandboxes.
It add the following features sandboxctl is missing :

*  List all sandboxes
*  Sandboxes status (configured, mounted, started, etc...)
*  If not downloaded, download the sets
*  Templates (to be confirmed)